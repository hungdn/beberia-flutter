class ResultLogin {
  final String token;

  ResultLogin({this.token});

  factory ResultLogin.fromJson(Map<String, dynamic> json) {
    return ResultLogin(
      token: json['display_name']
    );
  }
}