import 'dart:convert';
import 'dart:io';

import 'package:beberia_flutter/localizations.dart';
import 'package:beberia_flutter/screens/login/UserLogin.dart';
import 'package:flutter/material.dart';
import 'package:beberia_flutter/theme/style.dart';
import 'package:http/http.dart' as http;
import 'package:beberia_flutter/services/validations.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  BuildContext context;
  final GlobalKey<FormState> formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController scrollController = new ScrollController();
  bool autovalidate = false;
  Validations validations = new Validations();
  bool checkBox = false;
  String user = "";
  String pass = "";
  String token = "";

  Future<ResultLogin> fetchPost(String user, String pass, String token) async {
    final response = await http.post('http://api.beberia.me/api/v1/auth/login',
        body: {"display_name": user, "password": pass, "fcm_token": token});

    if (response.statusCode == 200) {
      print(json.decode(response.body));
      // If the server did return a 200 OK response, then parse the JSON.
      return ResultLogin.fromJson(json.decode(response.body));
    } else {
      // If the server did not return a 200 OK response, then throw an exception.
      throw Exception('Failed to load post');
    }
  }

  onPressed(String routeName) {
    Navigator.of(context).pushNamed(routeName);
  }

  void showInSnackBar(String value) {
    _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text(value)));
  }

  void _handleSubmitted() {
    final FormState form = formKey.currentState;
    if (!form.validate()) {
      autovalidate = true; // Start validating on every change.
      showInSnackBar('Please fix the errors in red before submitting.');
    } else {
      form.save();
      fetchPost(this.user, this.pass, this.token);
      //  Navigator.pushNamed(context, "/HomePage");
    }
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
    Validations validations = new Validations();
    return new Scaffold(
        key: _scaffoldKey,
        body: new SingleChildScrollView(
            controller: scrollController,
            child: new Container(
              child: Column(
                children: <Widget>[
                  new Container(
                    padding: new EdgeInsets.all(32.0),
                    child: new Column(
                      children: <Widget>[
                        new Container(
                            child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: Text(
                                  AppLocalizations.instance.text('login'),
                                  style: titleTextStyle),
                            )
                          ],
                        )),
                        new Container(
                          child: new Column(
                            children: <Widget>[
                              new Form(
                                key: formKey,
                                autovalidate: autovalidate,
                                child: new Column(
                                  children: <Widget>[
                                    new Container(
                                      margin: new EdgeInsets.only(top: 20.0),
                                      child: TextFormField(
                                        decoration: InputDecoration(
                                            hintText: AppLocalizations.instance
                                                .text('user_or_phone'),
                                            hintStyle: textHint),
                                        onSaved: (String value) {
                                          user = value;
                                        },
                                      ),
                                    ),
                                    new Container(
                                      margin: new EdgeInsets.only(top: 20.0),
                                      child: TextFormField(
                                        obscureText: true,
                                        decoration: InputDecoration(
                                            hintText: AppLocalizations.instance
                                                .text('password'),
                                            hintStyle: textHint),
                                        validator: validations.validatePassword,
                                        onSaved: (String value) {
                                          pass = value;
                                        },
                                      ),
                                    ),
                                    new Container(
                                        margin: new EdgeInsets.only(top: 20.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            new Row(
                                              children: <Widget>[
                                                new Checkbox(
                                                    value: false,
                                                    onChanged: (bool value) {
                                                      setState(() {
                                                        checkBox = value;
                                                      });
                                                    }),
                                                new Text(
                                                    AppLocalizations.instance
                                                        .text('save_password'),
                                                    style: textNormal12),
                                              ],
                                            ),
                                            new Text(
                                              AppLocalizations.instance
                                                  .text('forgot_password'),
                                              style: textNormal12,
                                            ),
                                          ],
                                        )),
                                    new Container(
                                        margin: new EdgeInsets.only(top: 20.0),
                                        child: SizedBox(
                                          width: double.infinity,
                                          height: 40,
                                          child: new RaisedButton(
                                            child: Text(
                                                AppLocalizations.instance
                                                    .text('login'),
                                                style: textNormal14Bold),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              side: BorderSide(
                                                  color:
                                                      const Color(0xfffff197)),
                                            ),
                                            color: const Color(0xfffff197),
                                            textColor: Colors.black,
                                            onPressed: _handleSubmitted,
                                          ),
                                        )),
                                    new Container(
                                      margin: new EdgeInsets.only(top: 20.0),
                                      child: Text(
                                          AppLocalizations.instance
                                              .text('or_login_with'),
                                          style: textNormal14),
                                    ),
                                    new Container(
                                      margin: new EdgeInsets.only(top: 20.0),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: <Widget>[
                                          Padding(
                                            padding: new EdgeInsets.only(
                                                right: 10.0),
                                            child: new Image.asset(
                                                "assets/icon/ic_fb.png",
                                                width: 32.0,
                                                height: 32.0),
                                          ),
                                          Padding(
                                            padding:
                                                new EdgeInsets.only(left: 10.0),
                                            child: new Image.asset(
                                                "assets/icon/ic_gg.png",
                                                width: 32.0,
                                                height: 32.0),
                                          )
                                        ],
                                      ),
                                    ),
                                    new Container(
                                        margin: new EdgeInsets.only(top: 20.0),
                                        child: SizedBox(
                                          width: double.infinity,
                                          height: 40,
                                          child: new RaisedButton(
                                            child: Text(
                                                AppLocalizations.instance
                                                    .text("create_account"),
                                                style: textNormal14Bold),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              side: BorderSide(
                                                  color:
                                                      const Color(0xfffece2f)),
                                            ),
                                            color: const Color(0xffffffff),
                                            textColor: Colors.black,
                                            onPressed: _handleSubmitted,
                                          ),
                                        )),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Image.asset("assets/background/bg_login.png")
                ],
              ),
            )));
  }
}
