
import 'package:beberia_flutter/localizations.dart';
import 'package:beberia_flutter/screens/main/diary.dart';
import 'package:beberia_flutter/screens/main/home.dart';
import 'package:beberia_flutter/screens/main/infor.dart';
import 'package:beberia_flutter/screens/main/secret.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashboardState();
  }
}

class _DashboardState extends State<Dashboard> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    Home(),
    Diary(),
    Secret(),
    Information()
  ];

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(

      home: new Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          showSelectedLabels: false,
          showUnselectedLabels: false,
          currentIndex:
          _currentIndex,
          type: BottomNavigationBarType.fixed,
          selectedItemColor: const Color(0xffee930b),
          onTap: onTabTapped,
          items: [
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage("assets/icon/ic_home.png")),
              activeIcon: ImageIcon(AssetImage("assets/icon/ic_home_active.png")),
              title: new Text(""),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage("assets/icon/ic_diary.png")),
              activeIcon: new ImageIcon(AssetImage("assets/icon/ic_diary_active.png")),
              title: new Text(""),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage("assets/icon/ic_secret_no.png")),
              activeIcon: new ImageIcon(AssetImage("assets/icon/ic_secret_active.png")),
              title: new Text(""),
            ),
            BottomNavigationBarItem(
              icon: new ImageIcon(AssetImage("assets/icon/ic_info.png")),
              activeIcon: new ImageIcon(AssetImage("assets/icon/ic_info_active.png")),
              title: new Text(""),
            )
          ],
        ),
        body: _children[_currentIndex],
      ),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
