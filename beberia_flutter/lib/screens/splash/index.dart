import 'package:beberia_flutter/screens/login/index.dart';
import "package:flutter/material.dart";

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    new Future.delayed(new Duration(seconds: 5), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => LoginScreen()),
              (Route route) => false);
    });

    // TODO: implement build
    return new Scaffold(
        body: new Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/background/bg_splash.png"),
                    fit: BoxFit.cover)),
            child: Center(
              child: Wrap(
                children: <Widget>[
                  new Image.asset(
                    "assets/icon/ic_beberia.png",
                    width: 195,
                    height: 53,
                  ),
                ],
              ),
            )));
  }
}
