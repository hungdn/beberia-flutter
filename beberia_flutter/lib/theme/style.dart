import 'package:flutter/material.dart';

TextStyle textStyle = const TextStyle(
    color: const Color(0XFFFFFFFF),
    fontSize: 16.0,
    fontWeight: FontWeight.normal);

ThemeData appTheme = new ThemeData(
  hintColor: Colors.white,
);

Color textFieldColor = const Color.fromRGBO(255, 255, 255, 0.1);

Color primaryColor = const Color(0xfffff197);

TextStyle buttonTextStyle = const TextStyle(
    color: const Color.fromRGBO(255, 255, 255, 0.8),
    fontSize: 14.0,
    fontFamily: "Roboto",
    fontWeight: FontWeight.bold);
TextStyle titleTextStyle = const TextStyle(
    color: Colors.black,
    fontSize: 24.0,
    fontFamily: "HelveticaNeue-Bold",
    fontWeight: FontWeight.bold);
TextStyle textNormal12 = const TextStyle(fontFamily: "HelveticaNeue",fontSize: 12,color: Colors.black);
TextStyle textNormal14 = const TextStyle(fontFamily: "HelveticaNeue",fontSize: 14,color: Colors.black);
TextStyle textNormal14Bold = const TextStyle(fontFamily: "HelveticaNeue",fontSize: 14,color: Colors.black,fontWeight: FontWeight.bold);
TextStyle textHint =
    const TextStyle(fontFamily: "HelveticaNeue", color: Colors.grey,fontSize: 14);
