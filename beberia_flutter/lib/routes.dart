import 'package:beberia_flutter/screens/main/index.dart';
import 'package:flutter/material.dart';
import 'package:beberia_flutter/screens/login/index.dart';
import 'package:beberia_flutter/screens/splash//index.dart';
import 'package:beberia_flutter/theme/style.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'localizations.dart';

class Routes {

  var routes = <String, WidgetBuilder>{
    "/login": (BuildContext context) => new LoginScreen(),
    "/HomePage": (BuildContext context) => new Dashboard()
  };

  Routes() {
    runApp(new MaterialApp(
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('en', ''),
        const Locale('es', ''),
      ],
      localeResolutionCallback:
          (Locale locale, Iterable<Locale> supportedLocales) {
        for (Locale supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode ||
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        return supportedLocales.first;
      },
      debugShowCheckedModeBanner: false,
      title: "Beberia",
      home: new SplashScreen(),
      theme: appTheme,
      routes: routes,
    ));
  }
}